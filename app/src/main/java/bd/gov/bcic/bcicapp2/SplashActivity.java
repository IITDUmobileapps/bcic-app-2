package bd.gov.bcic.bcicapp2;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;

import bd.gov.bcic.bcicapp2.data.AppDatabaseHelper;
import bd.gov.bcic.bcicapp2.utils.PreferenceHelper;

import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.CONTENT_BODY;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.CONTENT_IMAGE_URL;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.CONTENT_TITLE;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.HAS_SUBMENU;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.MENU_ID;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.MENU_NAME;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.MENU_TYPE;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.SUBMENU_ID;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.SUBMENU_NAME;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.TABLE_NAME;

/**
 * Created by Iftekhar on 4/27/2015.
 */
public class SplashActivity extends Activity {

    private PreferenceHelper preferenceHelper;

    private static final long SPLASH_PAUSE_IN_MILLISECOND = 2000;

    private class DataLoadAsyncTask extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... integers) {
            int resourceId = integers[0];
            loadAppDb(resourceId);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            preferenceHelper.setBoolean(Constants.PREF_KEY_IS_FIRST_LAUNCH, false);
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            finish();
        }
    }

    private void loadAppDb(int resourceId) {
        SQLiteDatabase database = new AppDatabaseHelper(this).getWritableDatabase();
        InputStream mInsertStream;
        BufferedReader bufferedReader;
        String readLine;
        try {
            mInsertStream = getResources().openRawResource(resourceId);
            bufferedReader = new BufferedReader(new InputStreamReader(mInsertStream));
            while ((readLine = bufferedReader.readLine()) != null) {
                readLine = readLine.replaceAll("\\\\n", "\\\n");
                String[] tokens = readLine.split("\\|");
                ContentValues values = new ContentValues();
                values.put(MENU_ID, tokens[0]);
                values.put(MENU_NAME, tokens[1]);
                values.put(MENU_TYPE, Integer.parseInt(tokens[2]));
                values.put(HAS_SUBMENU, Integer.parseInt(tokens[3]));
                values.put(SUBMENU_ID, tokens[4].equals("null") ? "" : tokens[4]);
                values.put(SUBMENU_NAME, tokens[5]);
                values.put(CONTENT_TITLE, tokens[6]);
                values.put(CONTENT_BODY, tokens[7]);
                values.put(CONTENT_IMAGE_URL, tokens[8]);
                database.insert(TABLE_NAME, null, values);
            }
        } catch (SQLiteConstraintException | IOException e) {
            e.printStackTrace();
        } finally {
            database.close();
        }
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    private void setSplashBitmap() {
        ImageView splash_image = (ImageView) findViewById(R.id.splash_image);
        if (splash_image.getDrawable() != null) {
            return;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getResources(), R.drawable.splash, options);
        options.inSampleSize = calculateInSampleSize(options, splash_image.getWidth(), splash_image.getHeight());
        options.inJustDecodeBounds = false;
        Bitmap splash_bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.splash, options);
        splash_image.setImageBitmap(splash_bitmap);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        setSplashBitmap();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_splash);

        preferenceHelper = PreferenceHelper.getInstance(this);
        if (preferenceHelper.getBoolean(Constants.PREF_KEY_IS_FIRST_LAUNCH, true)) {
            new DataLoadAsyncTask().execute(R.raw.app_pages);
        } else {
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                }
            }, SPLASH_PAUSE_IN_MILLISECOND);
        }
    }
}
