package bd.gov.bcic.bcicapp2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import bd.gov.bcic.bcicapp2.data.AppDatabaseHelper;
import bd.gov.bcic.bcicapp2.http.JsonTask;
import bd.gov.bcic.bcicapp2.ui.ContentFragment;
import bd.gov.bcic.bcicapp2.ui.SubmenuListFragment;
import bd.gov.bcic.bcicapp2.utils.Utilities;

import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.HAS_SUBMENU;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.MENU_ID;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.MENU_NAME;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.MENU_TYPE;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.SUBMENU_ID;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.TABLE_NAME;


public class MainActivity extends ActionBarActivity implements SubmenuListFragment.SubmenuItemClickCallback {

    private List<bd.gov.bcic.bcicapp2.model.Menu> menuList;
    private DrawerLayout mDrawerLayout;
    private Toolbar toolbar;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView mDrawerList;
    private Fragment mCurrentFragment;
    private ArrayAdapter<String> listAdapter;

    private List<bd.gov.bcic.bcicapp2.model.Menu> getAllMenu() {
        SQLiteDatabase sqLiteDatabase = new AppDatabaseHelper(this).getReadableDatabase();
        Cursor cursor = sqLiteDatabase.query(TABLE_NAME, new String[]{MENU_ID, MENU_NAME, MENU_TYPE, HAS_SUBMENU}
                , SUBMENU_ID + "==?", new String[]{""}, null, null, null);
        List<bd.gov.bcic.bcicapp2.model.Menu> menuList = new ArrayList<>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                bd.gov.bcic.bcicapp2.model.Menu menu = new bd.gov.bcic.bcicapp2.model.Menu(cursor.getString(0)
                        , cursor.getString(1), cursor.getInt(2), cursor.getInt(3) == 1);
                menuList.add(menu);
            }
            cursor.close();
        }
        sqLiteDatabase.close();
        return menuList;
    }

    private void setFragment(boolean keepBackStack) {
        if (mCurrentFragment == null) {
            return;
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (keepBackStack) {
            fragmentTransaction.add(R.id.fragment_container, mCurrentFragment).addToBackStack(null);
        } else {
            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); i++) {
                fragmentManager.popBackStack();
            }
            fragmentTransaction.replace(R.id.fragment_container, mCurrentFragment);
        }
        fragmentTransaction.commit();
    }

    private void setUpDrawerList(@NonNull List<bd.gov.bcic.bcicapp2.model.Menu> menuList) {
        if (listAdapter == null) {
            List<String> menuTitles = new ArrayList<>();
            for (bd.gov.bcic.bcicapp2.model.Menu menu : menuList) {
                menuTitles.add(menu.getMenuName());
            }
            listAdapter = new ArrayAdapter<>(this, R.layout.list_item_drawer, R.id.textView_menu, menuTitles);
            mDrawerList.setAdapter(listAdapter);
            mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        } else {
            listAdapter.clear();
            for (bd.gov.bcic.bcicapp2.model.Menu menu : menuList) {
                listAdapter.add(menu.getMenuName());
            }
            listAdapter.notifyDataSetChanged();
        }
    }

    private void initUi() {
        menuList = getAllMenu();
        if (menuList.isEmpty()) {
            return;
        }
        bd.gov.bcic.bcicapp2.model.Menu currentMenu = menuList.get(0);
        setTitle(currentMenu.getMenuName());
        setUpDrawerList(menuList);
        String current_menu_id = currentMenu.getMenuId();
        if (currentMenu.hasSubMenu()) {
            mCurrentFragment = SubmenuListFragment.getInstance(current_menu_id);
        } else {
            mCurrentFragment = ContentFragment.getInstance(ContentFragment.CONTENT_TYPE_MENU, current_menu_id);
        }
        setFragment(false);
    }

    private void updateAppData() {
        new JsonTask(this).requestUpdate();
        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (JsonTask.ACTION_JSON_TASK_COMPLETE.equals(intent.getAction())) {
                    initUi();
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter(JsonTask.ACTION_JSON_TASK_COMPLETE);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer);
        mDrawerList = (ListView) findViewById(R.id.listView_menu);
        toolbar = (Toolbar) findViewById(R.id.toolbar_home);
        setSupportActionBar(toolbar);

        initUi();

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                setFragment(false);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.action_update:
                if (Utilities.isNetworkAvailable(this)) {
                    updateAppData();
                } else {
                    Toast.makeText(this, Utilities.getBanglaSpannableString(this, getString(R.string.connect_to_internet))
                            , Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.START | Gravity.LEFT)) {
            mDrawerLayout.closeDrawers();
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void setTitle(CharSequence title) {
        getSupportActionBar().setTitle(Utilities.getBanglaSpannableString(this, title.toString()));
    }

    @Override
    public void onSubmenuItemClick(String submenuId) {
        mCurrentFragment = ContentFragment.getInstance(ContentFragment.CONTENT_TYPE_SUBMENU, submenuId);
        setFragment(true);
    }

    public class DrawerItemClickListener implements ListView.OnItemClickListener {

        private View lastSelectedItem;
        private Handler mHandler;

        public DrawerItemClickListener() {
            mHandler = new Handler();
        }

        private void checkItem(View item, int itemPos) {
            item.setBackgroundColor(getResources().getColor(R.color.grey_300));
            mDrawerList.setItemChecked(itemPos, true);
        }

        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            getSupportFragmentManager().beginTransaction().remove(mCurrentFragment).commit();
            bd.gov.bcic.bcicapp2.model.Menu selected_menu = menuList.get(position);
            String menu_id = selected_menu.getMenuId();
            if (selected_menu.hasSubMenu()) {
                mCurrentFragment = SubmenuListFragment.getInstance(menu_id);
            } else {
                mCurrentFragment = ContentFragment.getInstance(ContentFragment.CONTENT_TYPE_MENU, menu_id);
            }
            if (lastSelectedItem != null) {
                lastSelectedItem.setBackgroundColor(getResources().getColor(android.R.color.white));
            }
            checkItem(view, position);
            setTitle(selected_menu.getMenuName());
            lastSelectedItem = view;
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mDrawerLayout.closeDrawer(mDrawerList);
                }
            }, 150);
        }
    }
}
