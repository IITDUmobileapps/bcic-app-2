package bd.gov.bcic.bcicapp2.ui;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

import bd.gov.bcic.bcicapp2.R;
import bd.gov.bcic.bcicapp2.data.AppDatabaseHelper;
import bd.gov.bcic.bcicapp2.model.Submenu;

import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.MENU_ID;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.SUBMENU_ID;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.SUBMENU_NAME;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.TABLE_NAME;

/**
 * Created by Iftekhar on 4/25/2015.
 */
public class SubmenuListFragment extends ListFragment {

    private String menuId;
    private List<Submenu> submenuList;
    private ArrayAdapter<String> listAdapter;
    private SubmenuItemClickCallback itemClickCallback;

    private static final String ARG_MENU_ID = "_arg_menu_id";

    public SubmenuListFragment() {
    }

    private List<Submenu> getAllSubmenu(String menuId) {
        SQLiteDatabase sqLiteDatabase = new AppDatabaseHelper(getActivity()).getReadableDatabase();
        Cursor cursor = sqLiteDatabase.query(TABLE_NAME, new String[]{SUBMENU_ID, SUBMENU_NAME}
                , MENU_ID + "==? AND " + SUBMENU_ID + "!=?"
                , new String[]{menuId, ""}, null, null, null);
        List<Submenu> submenuList = new ArrayList<>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                Submenu submenu = new Submenu(cursor.getString(0), cursor.getString(1));
                submenuList.add(submenu);
            }
            cursor.close();
        }
        sqLiteDatabase.close();
        return submenuList;
    }

    public static SubmenuListFragment getInstance(String menuId) {
        SubmenuListFragment submenuListFragment = new SubmenuListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_MENU_ID, menuId);
        submenuListFragment.setArguments(args);
        return submenuListFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            itemClickCallback = (SubmenuItemClickCallback) activity;
        } catch (ClassCastException e) {
            Log.e(this.getTag(), "Parent activity must implement SubmenuItemClickCallback");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            menuId = args.getString(ARG_MENU_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_submenu_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        List<String> submenuTitles = new ArrayList<>();
        if (menuId != null) {
            submenuList = getAllSubmenu(menuId);
            for (Submenu submenu : submenuList) {
                submenuTitles.add(submenu.getSubmenuName());
            }
        }
        listAdapter = new ArrayAdapter<>(getActivity(), R.layout.list_item_submenu, R.id.textView_submenu
                , submenuTitles);
        setListAdapter(listAdapter);
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                itemClickCallback.onSubmenuItemClick(submenuList.get(position).getSubmenuId());
            }
        });
    }

    public interface SubmenuItemClickCallback {
        void onSubmenuItemClick(String submenuId);
    }
}
