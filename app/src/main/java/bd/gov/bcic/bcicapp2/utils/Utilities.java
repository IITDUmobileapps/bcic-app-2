package bd.gov.bcic.bcicapp2.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

import bd.gov.bcic.bcicapp2.R;
import bd.gov.bcic.bcicapp2.bangla.AndroidCustomFontSupport;
import bd.gov.bcic.bcicapp2.bangla.TypefaceSpan;

public class Utilities {

    public static final boolean isBuildAboveHoneyComb = Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB_MR2;
    public static final boolean isBanglaAvailable = isBanglaAvailable();

    private static Typeface typeface;

    public static Typeface getBanglaFont(Context context) {
        if (typeface == null) {
            typeface = Typeface.createFromAsset(context.getAssets(),
                    "fonts/" + context.getString(R.string.font_solaimanlipi));
        }
        return typeface;
    }

    public static SpannableString getBanglaSpannableString(Context context, String banglaText) {
        if (banglaText == null) {
            return new SpannableString("");
        }
        if (isBuildAboveHoneyComb) {
            SpannableString spannableString = new SpannableString(banglaText);
            if (isBanglaAvailable) {
                TypefaceSpan span = new TypefaceSpan(getBanglaFont(context));
                spannableString.setSpan(span, 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            return spannableString;
        }
        return AndroidCustomFontSupport.getCorrectedBengaliFormat(banglaText, getBanglaFont(context), -1);
    }

    private static boolean isBanglaAvailable() {
        Locale[] locales = Locale.getAvailableLocales();
        for (Locale locale : locales) {
            if (locale.getDisplayName().toLowerCase().contains("bengali")) {
                return true;
            }
        }
        return false;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectionManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectionManager.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isUrl(String text) {
        URL url = null;
        try {
            url = new URL(text);
        } catch (MalformedURLException e) {
            Log.i(text, "is not a URL");
        }
        if (url == null) {
            return false;
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public static void showLoadProgress(Context context,
                                        final View mainView, final View progressView, final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = context.getResources().getInteger(
                    android.R.integer.config_shortAnimTime);

            progressView.setVisibility(View.VISIBLE);
            progressView.animate().setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            progressView.setVisibility(show ? View.VISIBLE
                                    : View.GONE);
                        }
                    });

            mainView.setVisibility(View.VISIBLE);
            mainView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mainView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });
        } else {
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mainView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
