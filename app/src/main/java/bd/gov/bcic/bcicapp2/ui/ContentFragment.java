package bd.gov.bcic.bcicapp2.ui;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Pattern;

import bd.gov.bcic.bcicapp2.R;
import bd.gov.bcic.bcicapp2.data.AppDatabaseHelper;
import bd.gov.bcic.bcicapp2.model.Menu;
import bd.gov.bcic.bcicapp2.utils.Utilities;
import bd.gov.bcic.bcicapp2.view.BanglaTextView;

import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.CONTENT_BODY;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.CONTENT_IMAGE_URL;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.CONTENT_TITLE;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.MENU_ID;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.MENU_TYPE;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.SUBMENU_ID;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.TABLE_NAME;

/**
 * Created by Iftekhar on 4/24/2015.
 */
public class ContentFragment extends Fragment {

    public static final int CONTENT_TYPE_MENU = 0;
    public static final int CONTENT_TYPE_SUBMENU = 1;
    private int contentType;
    private String typeIdValue;
    private View loadProgressView;
    private View contentContainer;
    private View mapContainer;
    private BanglaTextView mTitle;
    private BanglaTextView mBody;
    private ImageView mImage;
    private WebView mWebView;
    private SupportMapFragment mMapFragment;

    private static final String KEY_ADDRESS = "address";
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_LONGITUDE = "longitude";
    private static final String ARG_TYPE = "_arg_type";
    private static final String ARG_TYPE_ID = "_arg_type_id_value";
    private static final HashMap<Integer, String> TYPE_TO_ID_FIELD_MAP = new HashMap<>();

    static {
        TYPE_TO_ID_FIELD_MAP.put(CONTENT_TYPE_MENU, MENU_ID);
        TYPE_TO_ID_FIELD_MAP.put(CONTENT_TYPE_SUBMENU, SUBMENU_ID);
    }

    public ContentFragment() {
    }

    private void setContent() {
        SQLiteDatabase sqLiteDatabase = new AppDatabaseHelper(getActivity()).getReadableDatabase();
        String typeIdField = TYPE_TO_ID_FIELD_MAP.get(contentType);
        Cursor cursor = sqLiteDatabase.query(TABLE_NAME, new String[]{MENU_TYPE, CONTENT_TITLE, CONTENT_BODY, CONTENT_IMAGE_URL}
                , typeIdField + "==?", new String[]{typeIdValue}, null, null, null, "1");
        if (cursor != null) {
            cursor.moveToFirst();
            int typeId = cursor.getInt(0);
            String body = cursor.getString(2);
            switch (typeId) {
                case Menu.TYPE_GENERAL:
                    if (Utilities.isUrl(body)) {
                        contentContainer.setVisibility(View.GONE);
                        mWebView.setVisibility(View.VISIBLE);
                        WebSettings webSettings = mWebView.getSettings();
                        webSettings.setJavaScriptEnabled(true);
                        mWebView.setWebViewClient(new WebViewClient() {
                            @Override
                            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                                handler.proceed();
                            }

                            @Override
                            public void onPageFinished(WebView view, String url) {
                                loadProgressView.setVisibility(View.GONE);
                            }

                            @Override
                            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                                loadProgressView.setVisibility(View.GONE);
                            }
                        });
                        mWebView.loadUrl(body);
                    } else {
                        mTitle.setText(cursor.getString(1));
                        mBody.setText(body);
                        Linkify.addLinks(mBody, Linkify.EMAIL_ADDRESSES);
                        Linkify.addLinks(mBody, Pattern.compile("[0][0-9]{10}|880\\-2\\-[0-9]{7}|৮৮০\\-২\\-[০-৯]{7}|[০-৯]{7}"), "tel:");
                        String imageUrl = cursor.getString(3);
                        if (imageUrl != null && !imageUrl.equals("")) {
                            ImageLoader imageLoader = ImageLoader.getInstance();
                            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory().cacheOnDisc()
                                    .resetViewBeforeLoading().build();
                            imageLoader.displayImage(imageUrl, mImage, options);
                        }
                        Utilities.showLoadProgress(getActivity(), contentContainer, loadProgressView, false);
                    }
                    break;
                case Menu.TYPE_CONTACT:
                    mTitle.setText(cursor.getString(1));
                    try {
                        JSONObject innerJson = new JSONObject(body);
                        String address = innerJson.getString(KEY_ADDRESS);
                        final double lat = innerJson.getDouble(KEY_LATITUDE);
                        final double lon = innerJson.getDouble(KEY_LONGITUDE);
                        final LatLng BCIC = new LatLng(lat, lon);
                        mBody.setText(address);
                        Linkify.addLinks(mBody, Linkify.EMAIL_ADDRESSES);
                        Linkify.addLinks(mBody, Pattern.compile("[0][0-9]{10}|880\\-2\\-[0-9]{7}|৮৮০\\-২\\-[০-৯]{7}|[০-৯]{7}"), "tel:");
                        mapContainer.setVisibility(View.VISIBLE);
                        final GoogleMap map = mMapFragment.getMap();
                        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                            @Override
                            public void onMapLoaded() {
                                CameraUpdate position = CameraUpdateFactory.newLatLng(BCIC);
                                CameraUpdate zoom = CameraUpdateFactory.zoomTo(15f);
                                map.moveCamera(position);
                                map.animateCamera(zoom);
                                map.addMarker(new MarkerOptions().position(BCIC));
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        Utilities.showLoadProgress(getActivity(), contentContainer, loadProgressView, false);
                    }
                    break;
                default:
                    break;
            }
            cursor.close();
        }
        sqLiteDatabase.close();
    }

    public static ContentFragment getInstance(int contentType, String typeId) {
        ContentFragment contentFragment = new ContentFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_TYPE, contentType);
        args.putString(ARG_TYPE_ID, typeId);
        contentFragment.setArguments(args);
        return contentFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            contentType = args.getInt(ARG_TYPE);
            typeIdValue = args.getString(ARG_TYPE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_content, container, false);
        mTitle = (BanglaTextView) root.findViewById(R.id.title);
        mBody = (BanglaTextView) root.findViewById(R.id.body);
        mImage = (ImageView) root.findViewById(R.id.image);
        mWebView = (WebView) root.findViewById(R.id.webview);
        mapContainer = root.findViewById(R.id.map_container);
        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        loadProgressView = root.findViewById(R.id.load_status);
        contentContainer = root.findViewById(R.id.content_container);
        Utilities.showLoadProgress(getActivity(), contentContainer, loadProgressView, true);
        return root;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (typeIdValue != null) {
            setContent();
        }
    }
}
