package bd.gov.bcic.bcicapp2.model;

/**
 * Created by Iftekhar on 4/25/2015.
 */
public class Submenu {

    private String submenuId;
    private String submenuName;

    public Submenu(String id, String name) {
        submenuId = id;
        submenuName = name;
    }

    public String getSubmenuId() {
        return submenuId;
    }

    public void setSubmenuId(String submenuId) {
        this.submenuId = submenuId;
    }

    public String getSubmenuName() {
        return submenuName;
    }

    public void setSubmenuName(String submenuName) {
        this.submenuName = submenuName;
    }
}
