package bd.gov.bcic.bcicapp2.http;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import bd.gov.bcic.bcicapp2.data.AppDatabaseHelper;

import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.CONTENT_BODY;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.CONTENT_IMAGE_URL;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.CONTENT_TITLE;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.HAS_SUBMENU;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.MENU_ID;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.MENU_NAME;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.MENU_TYPE;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.SUBMENU_ID;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.SUBMENU_NAME;
import static bd.gov.bcic.bcicapp2.data.AppDatabaseHelper.TABLE_NAME;

/**
 * Created by Iftekhar on 4/21/2015.
 */
public class JsonTask {

    private RequestQueue mRequestQueue;
    private Context mContext;
    private SQLiteDatabase database;

    private final String KEY_MENU = "Menu";
    private final String KEY_MENU_ID = "menu_id";
    private final String KEY_MENU_NAME = "menu_name";
    private final String KEY_MENU_TYPE = "menu_type";
    private final String KEY_SUBMENU = "Submenu";
    private final String KEY_SUBMENU_NAME = "submenu_name";
    private final String KEY_SUBMENU_ID = "submenu_id";
    private final String KEY_CONTENT = "content";
    private final String KEY_HAS_SUBMENU = "has_submenu";
    private final String KEY_APP_ID = "app_id";
    private final String APP_ID = "131";
    private final String KEY_TITLE = "title";
    private final String KEY_BODY = "body";
    private final String KEY_IMAGE = "image";
    private final String MENU_POST_URL = "http://api.national500apps.com/index.php?r=apiMenu/Getmenu&";
    private final String SUBMENU_POST_URL = "http://api.national500apps.com/index.php?r=apiMenu/Getsubmenu&";

    public static final String ACTION_JSON_TASK_COMPLETE = "_action_json_task_complete";

    public JsonTask(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
        database = new AppDatabaseHelper(mContext).getWritableDatabase();
    }

    private void requestSubmenu(final String menuId, final String menuName
            , final int menuType, final boolean closeDb) {
        Map<String, String> params = new HashMap<>();
        params.put(KEY_APP_ID, APP_ID);
        params.put(KEY_MENU_ID, menuId);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, SUBMENU_POST_URL
                , new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray result = (JSONArray) response.get(KEY_SUBMENU);
                    for (int i = 0; i < result.length(); i++) {
                        JSONObject submenu = (JSONObject) result.get(i);
                        String submenu_id = submenu.getString(KEY_SUBMENU_ID);
                        String submenu_name = submenu.getString(KEY_SUBMENU_NAME);
                        String content = submenu.getString(KEY_CONTENT);
                        JSONObject innerJson = new JSONObject(content.replaceAll("\n", "\\n"));
                        String title = innerJson.getString(KEY_TITLE);
                        String body = innerJson.getString(KEY_BODY);
                        String image = innerJson.getString(KEY_IMAGE);
                        ContentValues values = new ContentValues();
                        values.put(MENU_ID, menuId);
                        values.put(MENU_NAME, menuName);
                        values.put(MENU_TYPE, menuType);
                        values.put(HAS_SUBMENU, 0);
                        values.put(SUBMENU_ID, submenu_id);
                        values.put(SUBMENU_NAME, submenu_name);
                        values.put(CONTENT_TITLE, title);
                        values.put(CONTENT_BODY, body);
                        values.put(CONTENT_IMAGE_URL, image);
                        database.insert(TABLE_NAME, null, values);
                        Log.i("submenu name", submenu_name);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    if (closeDb) {
                        database.close();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        addToRequestQueue(jsonObjectRequest);
    }

    public void requestUpdate() {
        Map<String, String> params = new HashMap<>();
        params.put(KEY_APP_ID, APP_ID);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, MENU_POST_URL
                , new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray result = (JSONArray) response.get(KEY_MENU);
                    int len = result.length();
                    if (len <= 0) {
                        database.close();
                        Intent intent = new Intent(ACTION_JSON_TASK_COMPLETE);
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                        return;
                    }
                    database.execSQL("DELETE FROM " + TABLE_NAME);
                    for (int i = 0; i < len; i++) {
                        JSONObject menu = (JSONObject) result.get(i);
                        String menu_id = menu.getString(KEY_MENU_ID);
                        String menu_name = menu.getString(KEY_MENU_NAME);
                        int menu_type = menu.getInt(KEY_MENU_TYPE);
                        String content = menu.getString(KEY_CONTENT);
                        int has_submenu = menu.getInt(KEY_HAS_SUBMENU);
                        ContentValues values = new ContentValues();
                        values.put(MENU_ID, menu_id);
                        values.put(MENU_NAME, menu_name);
                        values.put(MENU_TYPE, menu_type);
                        values.put(HAS_SUBMENU, has_submenu);
                        values.put(SUBMENU_ID, "");
                        values.put(SUBMENU_NAME, "");
                        if (!content.equals("null") && !content.equals("")) {
                            JSONObject innerJson = new JSONObject(content.replaceAll("\n", "\\n"));
                            String title = innerJson.getString(KEY_TITLE);
                            String body = innerJson.getString(KEY_BODY);
                            String image = innerJson.getString(KEY_IMAGE);
                            values.put(CONTENT_TITLE, title);
                            values.put(CONTENT_BODY, body);
                            values.put(CONTENT_IMAGE_URL, image);
                        }
                        database.insert(TABLE_NAME, null, values);
                        Log.i("menu name", menu_name);
                        boolean is_last_obj = i == len - 1;
                        if (has_submenu == 1) {
                            requestSubmenu(menu_id, menu_name, menu_type, is_last_obj);
                        }
                        if (is_last_obj) {
                            Intent intent = new Intent(ACTION_JSON_TASK_COMPLETE);
                            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        addToRequestQueue(jsonObjectRequest);
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext
                    .getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

}
