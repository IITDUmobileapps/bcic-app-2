package bd.gov.bcic.bcicapp2.model;

/**
 * Created by Iftekhar on 4/25/2015.
 */
public class Menu {

    public static final int TYPE_GENERAL = 1;
    public static final int TYPE_PHOTO_GALLERY = 2;
    public static final int TYPE_VIDEO_GALLERY = 3;
    public static final int TYPE_CONTACT = 4;
    private boolean hasSubMenu;
    private int menuType;
    private String menuId;
    private String menuName;

    public Menu(String id, String name, int type, boolean subMenu) {
        menuId = id;
        menuName = name;
        menuType = type;
        hasSubMenu = subMenu;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public int getMenuType() {
        return menuType;
    }

    public void setMenuType(int menuType) {
        this.menuType = menuType;
    }

    public boolean hasSubMenu() {
        return hasSubMenu;
    }

    public void setHasSubMenu(boolean hasSubMenu) {
        this.hasSubMenu = hasSubMenu;
    }
}
