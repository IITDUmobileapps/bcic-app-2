package bd.gov.bcic.bcicapp2.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Iftekhar on 4/15/2015.
 */
public class AppDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "bcic_app_db.sqlite";
    private static final int VERSION = 1;

    public static final String TABLE_NAME = "app_pages";
    public static final String ID = "_id";
    public static final String MENU_ID = "menu_id";
    public static final String MENU_NAME = "manu_name";
    public static final String MENU_TYPE = "menu_type";
    public static final String HAS_SUBMENU = "has_submenu";
    public static final String SUBMENU_ID = "submenu_id";
    public static final String SUBMENU_NAME = "submenu_name";
    public static final String CONTENT_TITLE = "content_title";
    public static final String CONTENT_BODY = "content_body";
    public static final String CONTENT_IMAGE_URL = "content_image_url";

    public AppDatabaseHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + TABLE_NAME + "(" + ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + MENU_ID + " TEXT NOT NULL, " + MENU_NAME + " TEXT, "
                + MENU_TYPE + " INTEGER, " + HAS_SUBMENU + " INTEGER, "
                + SUBMENU_ID + " TEXT, " + SUBMENU_NAME + " TEXT, "
                + CONTENT_TITLE + " TEXT, " + CONTENT_BODY + " TEXT, "
                + CONTENT_IMAGE_URL + " TEXT" + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(this.getClass().getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
